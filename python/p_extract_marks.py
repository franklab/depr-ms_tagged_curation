import numpy as np
import json
import sys, os


parent_path=os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append('/opt/mountainlab/packages/pyms')
from mlpy import readmda, DiskReadMda, writemda16i, writemda64

processor_name='pyms.extract_marks'
processor_version='0.1'

def extract_marks(timeseries_path, firings_path, marks_out, markstimes_out):
    """
    extract marks intended for use with clusterless decoding.
    get timeseries values at firings times for all channels.
    Demetris Roumis March 2018.

    Parameters
    ----------
    timeseries : INPUT
        Path to filtered timeseries from which the events are extracted from (samplesxnchannels)
    firings : INPUT
        Path of input firings mda file
        The output of a sorting run is provided in a 2D array usually named “firings.mda”.
        The dimensions are RxL where L is the number of events and R is at least 3.
        Each column is a firing event.
        The first row contains the integer channels corresponding to the primary channels for each firing event.
        The second row contains the integer time points (1-based indexing) of the firing events
        The third row contains the integer labels, or the sorted spike types.
    marks_out : OUTPUT
         Path for output file for extracted marks
         2D nchannels x nevents int16
         values are center-aligned mark (voltage amplitude in the typical case) values from the timeseries
    markstimes_out : OUTPUT
        Path for output file for extracted marks times
        1D ntimes int64
    """
    #future: take in list of channels to ignore so that we can ignore spikes that have that channel as the primary using the label in
    firings = readmda(firings_path)
    timeseries = DiskReadMda(timeseries_path)
    nchannels, nsamples = timeseries.dims()
    markstimes = firings[1,:].astype('int')
    marks = []
    print('Marks extraction...')
    #future: call to c++ version
    for itime, timeInd in enumerate(markstimes):
        if timeInd < nsamples:
            marks.append(timeseries.readChunk(i1=timeInd, N1=nchannels))
        else:
            lastind = itime
            print('time index {timeInd} out of bounds for timeseries with {nsamples} samples'.format(timeInd=timeInd, nsamples=nsamples))
            break
    extracted_marks = np.stack(marks) #int16
    markstimes_used = markstimes[0:lastind] #int64
    write_extracted_marks(extracted_marks, marks_out, markstimes_used, markstimes_out)
    return True

def write_extracted_marks(extracted_marks, marks_out, markstimes_used, markstimes_out):
    #write to disk
    print('marks saving...')
    writemda16i(extracted_marks, marks_out)
    writemda64(markstimes_used, markstimes_out)
    return True

extract_marks.name = processor_name
extract_marks.version = processor_version
extract_marks.author = 'DRoumis'

if __name__ == '__main__':
    print ('no test yet')
