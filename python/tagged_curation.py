import os
import sys

parent_path=os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append('/opt/mountainlab/packages/pyms')

from mlpy import ProcessorManager

import p_add_curation_tags
import p_merge_burst_parents
import p_extract_marks

PM=ProcessorManager()

PM.registerProcessor(p_add_curation_tags.add_curation_tags)
PM.registerProcessor(p_merge_burst_parents.merge_burst_parents)
PM.registerProcessor(p_extract_marks.extract_marks)

if not PM.run(sys.argv):
    #exit(-1)
	pass
