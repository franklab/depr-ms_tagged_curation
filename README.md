# README #

# This repo is deprecated. contents have been moved to the packages subdir of 
https://bitbucket.org/franklab/franklab_ms_resources/src/master/

### This repo provides a processor for tagged curation of sort results
It also separates the curation step from the merging of burst parent clusters.


### To register these processors:

Put this package (or a link to this package) in `~/.mountainlab/packages/`
(Create this directory if it don't exist; this is where mountainsort will look for additional packages.)

The tagged_curation.py and the tagged_curation.mp scripts should be found automatically and will register the listed processors with mountainsort. Confirm this by running `mp-list-processors`, and look for the lines:
~~~
pyms.add_curation_tags
pyms.merge_burst_parents
~~~
	
### written by A K Gillespie, based on work from J Magland and J Chung